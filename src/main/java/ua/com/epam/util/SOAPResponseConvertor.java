package ua.com.epam.util;

import lombok.extern.log4j.Log4j2;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Log4j2
public class SOAPResponseConvertor {

    @SuppressWarnings("unchecked")
    public static <T> T responseToModel(SOAPMessage soapResponse, Class<T> t) throws SOAPException {
        Iterator<T> iterator = null;
        Unmarshaller unMarshaller = null;
        T response = null;
        List<SOAPBodyElement> soapBody = new ArrayList<>();
        try {
            iterator = (Iterator<T>) soapResponse.getSOAPBody().getChildElements();

            while (iterator.hasNext()) {
                Object obj = iterator.next();
                if (obj instanceof SOAPBodyElement) {
                    soapBody.add((SOAPBodyElement) obj);
                }
            }

            unMarshaller = JAXBContext.newInstance(t).createUnmarshaller();
            response = (T) unMarshaller.unmarshal(soapBody.get(0));
            log.info("Convert response to {}.", response.getClass().getSimpleName());
        } catch (SOAPException | JAXBException e) {
            log.error(String.format("Can't convert %s to %s", soapResponse, t.getClass().getSimpleName()), e);
            throw new SOAPException(e);
        }
        return response;
    }
}
