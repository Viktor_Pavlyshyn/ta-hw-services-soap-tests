package ua.com.epam.validator;

import org.apache.commons.collections4.CollectionUtils;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import java.util.List;

import static ua.com.epam.config.AssertConstant.*;

public class Validator {
    private SoftAssert softAssert = new SoftAssert();

    public <T> void isNotEmptyList(List<T> response) {

        Assert.assertTrue(CollectionUtils.isNotEmpty(response), AUTHOR_LIST_MSG);
    }

    public <T> void checkSortId(List<Long> response, String sortBy) {

        if ("asc".equals(sortBy.toLowerCase())) {
            for (int i = 0; i < response.size() - 1; i++) {
                if (response.get(i) > response.get(i + 1)) {
                    Assert.fail(String.format(SORT_BY_MSG, sortBy));
                    break;
                }
            }
        } else if ("desc".equals(sortBy.toLowerCase())) {
            for (int i = 0; i < response.size() - 1; i++) {
                if (response.get(i) < response.get(i + 1)) {
                    Assert.fail(String.format(SORT_BY_MSG, sortBy));
                    break;
                }
            }
        } else {
            Assert.fail(String.format("Invalid parameter - '%s' for sorting.", sortBy));
        }
    }

    public void checkFaultMsg(String faultString, String expectedParam) {
        Assert.assertEquals(expectedParam, faultString, FAULT_STRING_MSG);

    }

    public void checkFaultCode(String faultCode) {
        Assert.assertEquals(faultCode, FAULT_CODE, FAULT_CODE_MSG);
    }

    public <T> void checkResponseBody(T author, T response) {
        Assert.assertEquals(author, response, BODY_MSG);
    }
}
