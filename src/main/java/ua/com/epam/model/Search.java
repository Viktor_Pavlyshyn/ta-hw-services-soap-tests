package ua.com.epam.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class Search {
    @XmlElement(name = "orderType", namespace = "libraryService")
    private String orderType;
    @XmlElement(name = "page", namespace = "libraryService")
    private Integer page;
    @XmlElement(name = "pagination", namespace = "libraryService")
    private Boolean pagination;
    @XmlElement(name = "size", namespace = "libraryService")
    private Integer size;
}
