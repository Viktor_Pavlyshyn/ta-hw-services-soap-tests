package ua.com.epam.model.genre.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "getGenreRequest", namespace = "libraryService")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetGenreRequest implements Serializable {
    @XmlElement(name = "genreId", namespace = "libraryService")
    private Long genreId;
}
