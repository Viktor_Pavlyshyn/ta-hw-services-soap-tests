package ua.com.epam.model.genre.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.com.epam.model.genre.Genre;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "updateGenreRequest", namespace = "libraryService")
@XmlAccessorType(XmlAccessType.FIELD)
public class UpdateGenreRequest implements Serializable {
    @XmlElement(name = "genre", namespace = "libraryService")
    private Genre genre;
}
