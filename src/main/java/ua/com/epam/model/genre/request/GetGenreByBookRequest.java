package ua.com.epam.model.genre.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "getGenreByBookRequest", namespace = "libraryService")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetGenreByBookRequest implements Serializable {
    @XmlElement(name = "bookId", namespace = "libraryService")
    private Long bookId;
}
