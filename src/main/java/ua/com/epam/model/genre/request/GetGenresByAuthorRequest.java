package ua.com.epam.model.genre.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.com.epam.model.Search;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "getGenresByAuthorRequest", namespace = "libraryService")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetGenresByAuthorRequest implements Serializable {
    @XmlElement(name = "authorId", namespace = "libraryService")
    private Long authorId;
    @XmlElement(name = "search", namespace = "libraryService")
    private Search search;

}
