package ua.com.epam.model.genre.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.com.epam.model.genre.Genres;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "getGenreByBookResponse", namespace = "libraryService")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetGenreByBookResponse {
    @XmlElement(name = "genres", namespace = "libraryService")
    private Genres genres;
}
