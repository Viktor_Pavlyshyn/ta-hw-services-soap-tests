package ua.com.epam.model.genre;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.com.epam.model.author.Name;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class Genre {
    @XmlElement(name = "genreId", namespace = "libraryService")
    private Long genreId;
    @XmlElement(name = "genreName", namespace = "libraryService")
    private Name genreName;
    @XmlElement(name = "genreDescription", namespace = "libraryService")
    private String genreDescription;
}
