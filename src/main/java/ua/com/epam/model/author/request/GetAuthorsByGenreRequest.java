package ua.com.epam.model.author.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.com.epam.model.Search;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "getAuthorsByGenreRequest", namespace = "libraryService")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetAuthorsByGenreRequest implements Serializable {
    @XmlElement(name = "genreId", namespace = "libraryService")
    private Long genreId;
    @XmlElement(name = "search", namespace = "libraryService")
    private Search search;
}
