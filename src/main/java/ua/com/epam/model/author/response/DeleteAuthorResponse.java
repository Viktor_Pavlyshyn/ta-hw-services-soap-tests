package ua.com.epam.model.author.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "deleteAuthorResponse", namespace = "libraryService")
@XmlAccessorType(XmlAccessType.FIELD)
public class DeleteAuthorResponse {
    @XmlElement(name = "status", namespace = "libraryService")
    private String status;
}
