package ua.com.epam.model.author.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "getAuthorByBookRequest", namespace = "libraryService")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetAuthorByBookRequest implements Serializable {
    @XmlElement(name = "bookId", namespace = "libraryService")
    private Long bookId;
}
