package ua.com.epam.model.author.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.com.epam.model.Options;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "deleteAuthorRequest", namespace = "libraryService")
@XmlAccessorType(XmlAccessType.FIELD)
public class DeleteAuthorRequest implements Serializable {
    @XmlElement(name = "authorId", namespace = "libraryService")
    private Long authorId;
    @XmlElement(name = "options", namespace = "libraryService")
    private Options options;
}
