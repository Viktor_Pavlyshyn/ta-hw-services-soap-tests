package ua.com.epam.model.author;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class Name {
    @XmlElement(name = "first", namespace = "libraryService")
    private String first;
    @XmlElement(name = "second", namespace = "libraryService")
    private String second;
}
