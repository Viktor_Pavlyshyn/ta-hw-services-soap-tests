package ua.com.epam.model.author.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.com.epam.model.author.Authors;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "getAuthorsResponse", namespace = "libraryService")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetAuthorsResponse {
    @XmlElement(name = "authors", namespace = "libraryService")
    private Authors authors;
}
