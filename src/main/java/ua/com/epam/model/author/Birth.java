package ua.com.epam.model.author;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.com.epam.util.LocalDateAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class Birth {
    @XmlJavaTypeAdapter(value = LocalDateAdapter.class)
    @XmlElement(name = "date", namespace = "libraryService")
    private LocalDate date;
    @XmlElement(name = "country", namespace = "libraryService")
    private String country;
    @XmlElement(name = "city", namespace = "libraryService")
    private String city;
}
