package ua.com.epam.model.author.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.com.epam.model.Search;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "searchAuthorsRequest", namespace = "libraryService")
@XmlAccessorType(XmlAccessType.FIELD)
public class SearchAuthorsRequest implements Serializable {
    @XmlElement(name = "query", namespace = "libraryService")
    private String query;
    @XmlElement(name = "search", namespace = "libraryService")
    private Search search;
}
