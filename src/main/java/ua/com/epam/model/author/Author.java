package ua.com.epam.model.author;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class Author {
    @XmlElement(name = "authorId", namespace = "libraryService")
    private Long authorId;
    @XmlElement(name = "authorName", namespace = "libraryService")
    private Name authorName;
    @XmlElement(name = "nationality", namespace = "libraryService")
    private String nationality;
    @XmlElement(name = "birth", namespace = "libraryService")
    private Birth birth;
    @XmlElement(name = "authorDescription", namespace = "libraryService")
    private String authorDescription;
}
