package ua.com.epam.model.author.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.com.epam.model.author.Author;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "createAuthorRequest", namespace = "libraryService")
@XmlAccessorType(XmlAccessType.FIELD)
public class CreateAuthorRequest implements Serializable {
    @XmlElement(name = "author", namespace = "libraryService")
    private Author author;
}
