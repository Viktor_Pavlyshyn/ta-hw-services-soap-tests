package ua.com.epam.model.book.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "getBookRequest", namespace = "libraryService")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetBookRequest implements Serializable {
    @XmlElement(name = "bookId", namespace = "libraryService")
    private Long bookId;
}
