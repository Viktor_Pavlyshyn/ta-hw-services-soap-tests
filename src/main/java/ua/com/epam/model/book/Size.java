package ua.com.epam.model.book;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class Size {
    @XmlElement(name = "height", namespace = "libraryService")
    private Double height;
    @XmlElement(name = "width", namespace = "libraryService")
    private Double width;
    @XmlElement(name = "length", namespace = "libraryService")
    private Double length;
}