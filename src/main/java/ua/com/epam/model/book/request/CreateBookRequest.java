package ua.com.epam.model.book.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.com.epam.model.book.Book;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "createBookRequest", namespace = "libraryService")
@XmlAccessorType(XmlAccessType.FIELD)
public class CreateBookRequest implements Serializable {
    @XmlElement(name = "authorId", namespace = "libraryService")
    private Long authorId;
    @XmlElement(name = "genreId", namespace = "libraryService")
    private Long genreId;
    @XmlElement(name = "book", namespace = "libraryService")
    private Book book;
}
