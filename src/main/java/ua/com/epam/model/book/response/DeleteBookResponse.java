package ua.com.epam.model.book.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "deleteBookResponse", namespace = "libraryService")
@XmlAccessorType(XmlAccessType.FIELD)
public class DeleteBookResponse {
    @XmlElement(name = "status", namespace = "libraryService")
    private String status;
}
