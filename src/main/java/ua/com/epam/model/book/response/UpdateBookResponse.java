package ua.com.epam.model.book.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.com.epam.model.book.Book;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "updateBookResponse", namespace = "libraryService")
@XmlAccessorType(XmlAccessType.FIELD)
public class UpdateBookResponse {
    @XmlElement(name = "book", namespace = "libraryService")
    private Book book;
}
