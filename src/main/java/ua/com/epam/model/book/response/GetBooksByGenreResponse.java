package ua.com.epam.model.book.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.com.epam.model.book.Books;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "getBooksByGenreResponse", namespace = "libraryService")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetBooksByGenreResponse {
    @XmlElement(name = "books", namespace = "libraryService")
    private Books books;
}
