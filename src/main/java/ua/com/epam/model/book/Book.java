package ua.com.epam.model.book;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class Book {
    @XmlElement(name = "bookId", namespace = "libraryService")
    private Long bookId;
    @XmlElement(name = "bookName", namespace = "libraryService")
    private String bookName;
    @XmlElement(name = "bookLanguage", namespace = "libraryService")
    private String bookLanguage;
    @XmlElement(name = "bookDescription", namespace = "libraryService")
    private String bookDescription;
    @XmlElement(name = "additional", namespace = "libraryService")
    private Additional additional;
    @XmlElement(name = "publicationYear", namespace = "libraryService")
    private Long publicationYear;
}
