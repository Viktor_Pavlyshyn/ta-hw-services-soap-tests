package ua.com.epam.model.book.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "getBooksByAuthorAndGenreRequest", namespace = "libraryService")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetBooksByAuthorAndGenreRequest implements Serializable {
    @XmlElement(name = "authorId", namespace = "libraryService")
    private Long authorId;
    @XmlElement(name = "genreId", namespace = "libraryService")
    private Long genreId;
}
