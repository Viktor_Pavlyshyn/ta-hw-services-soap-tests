package ua.com.epam.model.fault;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "Fault")
@XmlAccessorType(XmlAccessType.FIELD)
public class Fault {
    @XmlElement(name = "faultcode")
    private String faultcode;
    @XmlElement(name = "faultstring")
    private String faultstring;
}
