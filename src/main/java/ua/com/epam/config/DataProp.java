package ua.com.epam.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static ua.com.epam.config.PropConstants.SOAP_ENDPOINT_URL;

public class DataProp {
    private Properties props = new Properties();

    public DataProp() {
        FileInputStream fis;

        try {
            fis = new FileInputStream("src/main/resources/data.properties");
            props.load(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getEndpoint() {
        return props.getProperty(SOAP_ENDPOINT_URL);
    }
}
