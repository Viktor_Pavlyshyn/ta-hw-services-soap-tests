package ua.com.epam.config;

public interface LibraryConstants {
    String LIBRARY_NAMESPACE = "lib";
    String LIBRARY_NAMESPACE_URI = "libraryService";
}
