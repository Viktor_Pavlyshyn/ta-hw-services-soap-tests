package ua.com.epam.config;

public interface AssertConstant {
    //Use String.format() for %s, %d.
    String FAULT_CODE = "SOAP-ENV:Server";
    String FAULT_STRING_SORT = "Sorting value %s is invalid, available sorting values: asc, desc";
    String FAULT_STRING_AUTHOR_EXIST = "Author with id %d already exists";
    String DELETE_STATUS_AUTHOR = "Successfully deleted author %d";
    String DELETE_EXIST_AUTHOR_STATUS = "Author with id %d not found";

    String FAULT_CODE_MSG = "Incorrect fault code.";
    String FAULT_STRING_MSG = "Incorrect string code.";
    String BODY_MSG = "Incorrect response body.";
    String SORT_BY_MSG = "Response isn't sorted by '%s'.";
    String AUTHOR_LIST_MSG = "Author list is empty.";
}
