package ua.com.epam.service;

import lombok.extern.log4j.Log4j2;
import ua.com.epam.client.SOAPClient;
import ua.com.epam.model.Options;
import ua.com.epam.model.Search;
import ua.com.epam.model.author.Author;
import ua.com.epam.model.author.request.*;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

@Log4j2
public class AuthorService {
    private SOAPClient client;

    public AuthorService(SOAPClient client) {
        this.client = client;
    }

    public SOAPMessage deleteAuthor(Long authorId, Options options) throws SOAPException {
        SOAPMessage response = null;

        try {
            DeleteAuthorRequest modelRequest = new DeleteAuthorRequest();
            modelRequest.setAuthorId(authorId);
            modelRequest.setOptions(options);

            log.info("Start deleting author by id - {}.", authorId);
            response = client.sendRequest(modelRequest);
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't delete Author by id - {}.", authorId);
            throw e;
        }
        return response;
    }

    public SOAPMessage getAuthorByBook(Long bookId) throws SOAPException {
        SOAPMessage response = null;

        try {
            GetAuthorByBookRequest modelRequest = new GetAuthorByBookRequest(bookId);

            log.info("Start getting author by book id - {}.", bookId);
            response = client.sendRequest(modelRequest);
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't get author by book id - {}.", bookId);
            throw e;
        }
        return response;
    }

    public SOAPMessage getAuthorsByGenre(Long genreId, Search search) throws SOAPException {
        SOAPMessage response = null;

        try {
            GetAuthorsByGenreRequest modelRequest = new GetAuthorsByGenreRequest();
            modelRequest.setGenreId(genreId);
            modelRequest.setSearch(search);

            log.info("Start getting author by genre id - {}.", genreId);
            response = client.sendRequest(modelRequest);
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't get author by genre id - {}.", genreId);
            throw e;
        }
        return response;
    }

    public SOAPMessage getAuthor(Long authorId) throws SOAPException {
        SOAPMessage response = null;
        try {
            GetAuthorRequest modelRequest = new GetAuthorRequest(authorId);

            log.info("Start getting author by id - {}.", authorId);
            response = client.sendRequest(modelRequest);
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't get author by id - {}.", authorId);
            throw e;
        }
        return response;
    }

    public SOAPMessage searchAuthors(String query, Search search) throws SOAPException {
        SOAPMessage response = null;
        try {
            SearchAuthorsRequest modelRequest = new SearchAuthorsRequest();
            modelRequest.setQuery(query);
            modelRequest.setSearch(search);

            log.info("Start searching author by name or surname - {}.", query);
            response = client.sendRequest(modelRequest);
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't search author by author by by name or surname - {}.", query);
            throw e;
        }
        return response;
    }

    public SOAPMessage updateAuthor(Author author) throws SOAPException {
        SOAPMessage response = null;
        try {
            UpdateAuthorRequest modelRequest = new UpdateAuthorRequest(author);

            log.info("Start updating author - {}.", author);
            response = client.sendRequest(modelRequest);
            log.info("Successful return response.");
            ;
        } catch (Exception e) {
            log.error("Can't update Author - {}.", author);
            throw e;
        }
        return response;
    }

    public SOAPMessage createAuthor(Author author) throws SOAPException {
        SOAPMessage response = null;
        try {
            CreateAuthorRequest modelRequest = new CreateAuthorRequest(author);

            log.info("Start creating author - {}.", author);
            response = client.sendRequest(modelRequest);
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't create Author - {}.", author);
            throw e;
        }
        return response;
    }

    public SOAPMessage getAuthors(Search search) throws SOAPException {
        SOAPMessage response = null;

        try {
            GetAuthorsRequest modelRequest = new GetAuthorsRequest(search);

            log.info("Start getting all authors.");
            response = client.sendRequest(modelRequest);
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't get authors.");
            throw e;
        }
        return response;
    }
}
