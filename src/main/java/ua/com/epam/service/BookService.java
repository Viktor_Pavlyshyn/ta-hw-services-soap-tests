package ua.com.epam.service;

import lombok.extern.log4j.Log4j2;
import ua.com.epam.client.SOAPClient;
import ua.com.epam.model.Search;
import ua.com.epam.model.book.Book;
import ua.com.epam.model.book.request.*;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

@Log4j2
public class BookService {
    private SOAPClient client;

    public BookService(SOAPClient client) {
        this.client = client;
    }

    public SOAPMessage deleteBook(Long bookId) throws SOAPException {
        SOAPMessage response = null;
        try {
            DeleteBookRequest modelRequest = new DeleteBookRequest(bookId);

            log.info("Start deleting book by id - {}.", bookId);
            response = client.sendRequest(modelRequest);
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't create book by id - {}.", bookId);
            throw e;
        }
        return response;
    }

    public SOAPMessage getBooksByAuthor(Long authorId, Search search) throws SOAPException {
        SOAPMessage response = null;
        try {
            GetBooksByAuthorRequest modelRequest = new GetBooksByAuthorRequest();
            modelRequest.setAuthorId(authorId);
            modelRequest.setSearch(search);

            log.info("Start getting book by author id - {}.", authorId);
            response = client.sendRequest(modelRequest);
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't get book by author id - {}.", authorId);
            throw e;
        }
        return response;
    }

    public SOAPMessage getBooksByAuthorAndGenre(Long authorId, Long genreId) throws SOAPException {
        SOAPMessage response = null;
        try {
            GetBooksByAuthorAndGenreRequest modelRequest = new GetBooksByAuthorAndGenreRequest();
            modelRequest.setAuthorId(authorId);
            modelRequest.setGenreId(genreId);

            log.info("Start getting book by author id - {} and genre id - {}.", authorId, genreId);
            response = client.sendRequest(modelRequest);
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't get book by author id - {} and genre id - {}.", authorId, genreId);
            throw e;
        }
        return response;
    }

    public SOAPMessage getBooksByGenre(Long genreId, Search search) throws SOAPException {
        SOAPMessage response = null;
        try {
            GetBooksByGenreRequest modelRequest = new GetBooksByGenreRequest();
            modelRequest.setGenreId(genreId);
            modelRequest.setSearch(search);

            log.info("Start getting book by genre id - {}.", genreId);
            response = client.sendRequest(modelRequest);
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't get book by genre id - {}.", genreId);
            throw e;
        }
        return response;
    }

    public SOAPMessage getBook(Long bookId) throws SOAPException {
        SOAPMessage response = null;
        try {
            GetBookRequest modelRequest = new GetBookRequest(bookId);

            log.info("Start deleting book by id - {}.", bookId);
            response = client.sendRequest(modelRequest);
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't create book by id - {}.", bookId);
            throw e;
        }
        return response;
    }

    public SOAPMessage searchBooks(String query, Search search) throws SOAPException {
        SOAPMessage response = null;
        try {
            SearchBooksRequest modelRequest = new SearchBooksRequest();
            modelRequest.setQuery(query);
            modelRequest.setSearch(search);

            log.info("Start searching book by name - {}.", query);
            response = client.sendRequest(modelRequest);
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't search book by name - {}.", query);
            throw e;
        }
        return response;
    }

    public SOAPMessage updateBook(Book book) throws SOAPException {
        SOAPMessage response = null;
        try {
            UpdateBookRequest modelRequest = new UpdateBookRequest(book);

            log.info("Start updating book - {}.", book);
            response = client.sendRequest(modelRequest);
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't update book - {}.", book);
            throw e;
        }
        return response;
    }

    public SOAPMessage createBook(Long authorId, Long genreId, Book book) throws SOAPException {
        SOAPMessage response = null;
        try {
            CreateBookRequest modelRequest = new CreateBookRequest();
            modelRequest.setAuthorId(authorId);
            modelRequest.setGenreId(genreId);
            modelRequest.setBook(book);

            log.info("Start creating book - {}.", book);
            response = client.sendRequest(modelRequest);
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't create book - {}.", book);
            throw e;
        }
        return response;
    }

    public SOAPMessage getBooks(Search search) throws SOAPException {
        SOAPMessage response = null;
        try {
            GetBooksRequest modelRequest = new GetBooksRequest(search);

            log.info("Start getting all books.");
            response = client.sendRequest(modelRequest);
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't get all books.");
            throw e;
        }
        return response;
    }
}
