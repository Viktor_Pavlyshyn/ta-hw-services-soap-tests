package ua.com.epam.service;

import lombok.extern.log4j.Log4j2;
import ua.com.epam.client.SOAPClient;
import ua.com.epam.model.Options;
import ua.com.epam.model.Search;
import ua.com.epam.model.genre.Genre;
import ua.com.epam.model.genre.request.*;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

@Log4j2
public class GenreService {
    private SOAPClient client;

    public GenreService(SOAPClient client) {
        this.client = client;
    }

    public SOAPMessage deleteGenre(Long genreId, Options options) throws SOAPException {
        SOAPMessage response = null;
        try {
            DeleteGenreRequest modelRequest = new DeleteGenreRequest();
            modelRequest.setGenreId(genreId);
            modelRequest.setOptions(options);

            log.info("Start deleting genre by id - {}.", genreId);
            response = client.sendRequest(modelRequest);
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't delete genre by id - {}.", genreId);
            throw e;
        }
        return response;
    }

    public SOAPMessage getGenreByBook(Long bookId) throws SOAPException {
        SOAPMessage response = null;
        try {
            GetGenreByBookRequest modelRequest = new GetGenreByBookRequest(bookId);

            log.info("Start getting  genre by book id - {}.", bookId);
            response = client.sendRequest(modelRequest);
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't get genre by book id - {}..", bookId);
            throw e;
        }
        return response;
    }

    public SOAPMessage getGenresByAuthor(Long authorId, Search search) throws SOAPException {
        SOAPMessage response = null;
        try {
            GetGenresByAuthorRequest modelRequest = new GetGenresByAuthorRequest();
            modelRequest.setAuthorId(authorId);
            modelRequest.setSearch(search);

            log.info("Start getting all genres by author id - {}.", authorId);
            response = client.sendRequest(modelRequest);
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't get all genres by author id - {}..", authorId);
            throw e;
        }
        return response;
    }

    public SOAPMessage getGenre(Long genreId) throws SOAPException {
        SOAPMessage response = null;
        try {
            GetGenreRequest modelRequest = new GetGenreRequest(genreId);

            log.info("Start getting genre by id - {}.", genreId);
            response = client.sendRequest(modelRequest);
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't get genre by id - {}.", genreId);
            throw e;
        }
        return response;
    }

    public SOAPMessage searchGenres(String genreName, Genre genre) throws SOAPException {
        SOAPMessage response = null;
        try {
            SearchGenresRequest modelRequest = new SearchGenresRequest();
            modelRequest.setQuery(genreName);
            modelRequest.setGenre(genre);

            log.info("Start searching genre by name- {}.", genreName);
            response = client.sendRequest(modelRequest);
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't search genre by author by by name - {}.", genreName);
            throw e;
        }
        return response;
    }

    public SOAPMessage updateGenre(Genre genre) throws SOAPException {
        SOAPMessage response = null;
        try {
            UpdateGenreRequest modelRequest = new UpdateGenreRequest(genre);

            log.info("Start updating author - {}.", genre);
            response = client.sendRequest(modelRequest);
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't update Author - {}.", genre);
            throw e;
        }
        return response;
    }

    public SOAPMessage createGenre(Genre genre) throws SOAPException {
        SOAPMessage response = null;
        try {
            CreateGenreRequest modelRequest = new CreateGenreRequest(genre);

            log.info("Start creating genre - {}.", genre);
            response = client.sendRequest(modelRequest);
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't create genre - {}.", genre);
            throw e;
        }
        return response;
    }

    public SOAPMessage getGenres(Search search) throws SOAPException {
        SOAPMessage response = null;
        try {
            GetGenresRequest modelRequest = new GetGenresRequest(search);

            log.info("Start getting all genres.");
            response = client.sendRequest(modelRequest);
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't get genres.");
            throw e;
        }
        return response;
    }
}
