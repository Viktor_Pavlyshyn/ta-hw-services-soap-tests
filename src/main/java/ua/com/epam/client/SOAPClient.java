package ua.com.epam.client;

import lombok.extern.log4j.Log4j2;
import ua.com.epam.config.DataProp;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.soap.*;

import static ua.com.epam.config.LibraryConstants.LIBRARY_NAMESPACE;
import static ua.com.epam.config.LibraryConstants.LIBRARY_NAMESPACE_URI;

@Log4j2
public class SOAPClient {
    DataProp dataProp = new DataProp();
    private String soapEndpointUrl = dataProp.getEndpoint();

    public <T> SOAPMessage sendRequest(T t) throws SOAPException {
        SOAPMessage soapMessage = null;
        try {
            MessageFactory messageFactory = MessageFactory.newInstance();
            soapMessage = messageFactory.createMessage();

            SOAPPart soapPart = soapMessage.getSOAPPart();

            SOAPEnvelope envelope = soapPart.getEnvelope();

            envelope.addNamespaceDeclaration(LIBRARY_NAMESPACE, LIBRARY_NAMESPACE_URI);

            SOAPBody soapBody = envelope.getBody();

            JAXBContext jc = JAXBContext.newInstance(t.getClass());
            Marshaller marshaller = jc.createMarshaller();
            marshaller.marshal(t, soapBody);

            soapMessage.saveChanges();

            log.info("Create SOAPEnvelop for model - {}", t);
        } catch (SOAPException | JAXBException e) {
            log.error(String.format("Can't create SOAPEnvelop for model - %s.", t), e);
            throw new SOAPException(e);
        }
        return callSoapWebService(soapMessage);
    }

    public SOAPMessage callSoapWebService(SOAPMessage soapRequest) throws SOAPException {
        SOAPMessage soapResponse = null;
        SOAPConnection soapConnection = null;

        SOAPConnectionFactory soapConnectionFactory = null;
        try {
            soapConnectionFactory = SOAPConnectionFactory.newInstance();
            soapConnection = soapConnectionFactory.createConnection();

            soapResponse = soapConnection.call(soapRequest, soapEndpointUrl);

            log.info("Create SOAPResponse.");

        } catch (SOAPException e) {
            log.error("\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
            throw new SOAPException(e);
        } finally {
            soapConnection.close();
        }
        return soapResponse;
    }
}
