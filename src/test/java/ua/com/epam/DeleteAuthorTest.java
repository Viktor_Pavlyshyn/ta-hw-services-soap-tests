package ua.com.epam;

import org.testng.annotations.Test;
import ua.com.epam.model.Options;
import ua.com.epam.model.author.Author;
import ua.com.epam.model.author.response.DeleteAuthorResponse;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;

import static ua.com.epam.config.AssertConstant.DELETE_EXIST_AUTHOR_STATUS;
import static ua.com.epam.config.AssertConstant.DELETE_STATUS_AUTHOR;
import static ua.com.epam.util.SOAPResponseConvertor.responseToModel;

public class DeleteAuthorTest extends BaseTest {

    @Test
    public void deleteAuthor() throws SOAPException {

        Author author = libraryData.getAuthorWithBaseData();
        Options options = libraryData.getOptionsWithBaseData();
        authorService.createAuthor(author);

        String response = responseToModel(authorService.deleteAuthor(author.getAuthorId(), options)
                , DeleteAuthorResponse.class).getStatus();

        validator.checkResponseBody(String.format(DELETE_STATUS_AUTHOR, author.getAuthorId()), response);
    }

    @Test
    public void deleteNotExistAuthor() throws SOAPException {
        Author author = libraryData.getAuthorWithBaseData();
        Long id = author.getAuthorId();
        Options options = libraryData.getOptionsWithBaseData();

        authorService.createAuthor(author);
        authorService.deleteAuthor(id, options);

        SOAPFault response = authorService.deleteAuthor(id, options)
                .getSOAPBody().getFault();

        String code = response.getFaultCode();
        validator.checkFaultCode(code);

        String faultString = response.getFaultString();
        validator.checkFaultMsg(faultString,
                String.format(DELETE_EXIST_AUTHOR_STATUS, id));

    }
}
