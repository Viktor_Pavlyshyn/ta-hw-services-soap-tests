package ua.com.epam;

import org.testng.annotations.Test;
import ua.com.epam.model.Search;
import ua.com.epam.model.author.Author;
import ua.com.epam.model.author.response.GetAuthorsResponse;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import java.util.List;
import java.util.stream.Collectors;

import static ua.com.epam.config.AssertConstant.FAULT_STRING_SORT;
import static ua.com.epam.util.SOAPResponseConvertor.responseToModel;

public class GetAuthorsTest extends BaseTest {

    @Test
    public void getAllAuthors() throws SOAPException {
        Search search = libraryData.getSearchWithBaseData();

        List<Author> lisAuthor = responseToModel(authorService.getAuthors(search)
                , GetAuthorsResponse.class).getAuthors().getAuthor();

        validator.isNotEmptyList(lisAuthor);
    }

    @Test
    public void getAllAuthorsByFalseParam() throws SOAPException {

        Search search = libraryData.getSearchWithBaseData();
        search.setOrderType("test");

        SOAPFault response = authorService.getAuthors(search).getSOAPBody().getFault();

        String code = response.getFaultCode();
        validator.checkFaultCode(code);

        String faultString = response.getFaultString();
        validator.checkFaultMsg(faultString, String.format(FAULT_STRING_SORT, "test"));
    }

    @Test
    public void getAllAuthorsCheckSort() throws SOAPException {

        Search search = libraryData.getSearchWithBaseData();

        List<Author> lisAuthor = responseToModel(authorService.getAuthors(search)
                , GetAuthorsResponse.class).getAuthors().getAuthor();

        List<Long> authorIdList = lisAuthor.stream().map(Author::getAuthorId).collect(Collectors.toList());

        validator.checkSortId(authorIdList, "asc");
    }
}
