package ua.com.epam;

import org.testng.annotations.Test;
import ua.com.epam.model.author.Author;
import ua.com.epam.model.author.response.CreateAuthorResponse;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;

import static ua.com.epam.config.AssertConstant.FAULT_STRING_AUTHOR_EXIST;
import static ua.com.epam.util.SOAPResponseConvertor.responseToModel;

public class CreateAuthorTest extends BaseTest {

    @Test
    public void createAuthor() throws SOAPException {
        Author author = libraryData.getAuthorWithBaseData();

        Author response = responseToModel(authorService.createAuthor(author)
                , CreateAuthorResponse.class).getAuthor();

        validator.checkResponseBody(author, response);

        authorService.deleteAuthor(author.getAuthorId(), libraryData.getOptionsWithBaseData());
    }

    @Test
    public void createExistAuthor() throws SOAPException {

        Author author = libraryData.getAuthorWithBaseData();
        authorService.createAuthor(author);

        SOAPFault response = authorService.createAuthor(author).getSOAPBody().getFault();

        String code = response.getFaultCode();
        validator.checkFaultCode(code);

        String faultString = response.getFaultString();
        validator.checkFaultMsg(faultString,
                String.format(FAULT_STRING_AUTHOR_EXIST, author.getAuthorId()));

        authorService.deleteAuthor(author.getAuthorId(), libraryData.getOptionsWithBaseData());
    }
}
