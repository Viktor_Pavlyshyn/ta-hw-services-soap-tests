package ua.com.epam.data;

import ua.com.epam.model.Options;
import ua.com.epam.model.Search;
import ua.com.epam.model.author.Author;
import ua.com.epam.model.author.Birth;
import ua.com.epam.model.author.Name;
import ua.com.epam.model.book.Additional;
import ua.com.epam.model.book.Book;
import ua.com.epam.model.book.Size;
import ua.com.epam.model.genre.Genre;

import java.time.LocalDate;
import java.util.Random;

public class LibraryData {

    public Long getRandomId() {
        Random rd = new Random();
        return (long) (rd.nextInt((9999999 - 1) + 1) + 1);
    }

    public Author getAuthorWithBaseData() {
        Name name = new Name();
        name.setFirst("a");
        name.setSecond("a");

        Birth birth = new Birth();
        birth.setDate(LocalDate.ofEpochDay(1111 - 11 - 11));
        birth.setCountry("a");
        birth.setCity("a");

        Author author = new Author();
        author.setAuthorId(getRandomId());
        author.setAuthorName(name);
        author.setNationality("a");
        author.setBirth(birth);
        author.setAuthorDescription("a");
        return author;
    }

    public Book getBookWithBaseData() {
        Size size = new Size();
        size.setHeight(1.1);
        size.setWidth(1.1);
        size.setLength(1.1);

        Additional additional = new Additional();
        additional.setPageCount(1L);
        additional.setSize(size);

        Book book = new Book();
        book.setBookId(getRandomId());
        book.setBookName("a");
        book.setBookLanguage("a");
        book.setBookDescription("a");
        book.setAdditional(additional);
        return book;
    }

    public Genre getGenreWithBaseData() {
        Name name = new Name();
        name.setFirst("a");
        name.setSecond("a");

        Genre genre = new Genre();
        genre.setGenreId(getRandomId());
        genre.setGenreName(name);
        genre.setGenreDescription("a");
        return genre;
    }

    public Search getSearchWithBaseData() {
        Search search = new Search();
        search.setOrderType("asc");
        search.setPage(1);
        search.setPagination(true);
        search.setSize(10);

        return search;
    }

    public Options getOptionsWithBaseData() {

        Options options = new Options();
        options.setForcibly(false);

        return options;
    }
}
